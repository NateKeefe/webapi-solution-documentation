
/**
 * Copyright 2017, Scribe Software, Inc.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * See License:  http://info.scribesoft.com/rs/957-WMM-217/images/Scribe%20Labs%20license%20agreement%202015.pdf
 * 
 * This is a simple Documentation Integration Tool developed in Google Sheets against Scribe's Rest API
 */



//Global Variables
// Credentials to call Scribe API
    var APIURL; 
    var USER; 


//Setting Up the Menu System in Google Sheets Container 
function onOpen() {
  SpreadsheetApp.getUi() // Or DocumentApp
      .createMenu('Scribe')
        .addItem('Document Solutions', 'openDialog')
        .addItem('Reset', 'doReset')
      .addToUi(); 
}

//Menu entry point functions

function openDialog() {
    var html = HtmlService.createTemplateFromFile('DocumentSolutions')
    .evaluate() // evaluate MUST come before setting the NATIVE mode
    .setSandboxMode(HtmlService.SandboxMode.NATIVE).setHeight(500);
     SpreadsheetApp.getUi() // 
    .showModalDialog(html, 'Scribe Documentation Tool');
}

function include(filename) {
//Enables the loading of CSS and JS files both name .html extension based on Google Script Include Limit
  return HtmlService.createHtmlOutputFromFile(filename)
  
      .getContent();
}


function formProcessor(formObject){
  //This function is invoked from the Form in DocumentSolutions.html via the javascript.html handler
  doReset(); //Housekeeping of the workbook prior to each run 
  APIURL = formObject.environment; 
  var form_context = formObject.form_context;
  var orgId = formObject.org; 
  var solutionId = formObject.solution; 
  var errorRuns = formObject.errorRuns; 
  var org; 
  var orgs = []; 
  var block; 
  var blocks =[]; 

  USER = {username: formObject.username, password: formObject.password};  
  
  
  // Form Context is used passed between client and server for simple state management 
  switch (form_context) {
    case "Connect":
        //Logger.log("Called Connect for user " + USER.username ); // Useful debug trace
        orgs = getScribeOrgs(); 
        return orgs;
        break; 
    case "Solution":
         //Logger.log("Called Solutions for user " + USER.username + "With org ID - " + orgId); // Useful debug trace
         solutions =  getScribeSolutionsListByOrgId(orgId);
         return solutions; 
         break; 
      case "Document":
         //Logger.log("Called Document for user " + USER.username + "With org ID - " + orgId + " for Solution " + solutionId);// Useful debug trace
         org = getScribeOrg(orgId); 
         outputOrg(org); 
         // Generate Solution File 
         solution = getScribeSolution(org, solutionId);
         createSheet(solution.name); 
         outputSolution(solution); 
         //Output the Maps for the Solution 
         maps = getScribeMaps(org, solution); 
    
        //For each Map in Maps Create A WorkSheet
        for (var i =0; i < maps.length; i++){
            map = maps[i]; 
            outputMap(map); 
        //Get the Blocks in the Current Map 
        blocks = getScribeBlocks(org.id, solution.id, map.id);
        outputBlocks(blocks); 
        }
    
    //Building the List of Solutions available for Documentation and sending back to the DocumentSolutions Form 
    orgs = getScribeOrgs();    
    } 
   
    return orgs
  
  }


//========================= SCRIBE API GET FUNCTIONS =============================
  
function getScribeBlocks(orgId, solutionId, mapId){
    var method; 
    var map; 
    var dataSet = []; 
    var url = APIURL + orgId + "/solutions/" + solutionId + "/maps/" + mapId; 
  
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
  
    var block; 
    var blocks = [];
    
    blocks = dataSet.blocks; 
   
    for (var i = 0; i < dataSet.length; i++) {
    block = dataSet[i];
    blocks.push({id: block.id, 
               name: block.name,
               entity: block.entity,
               label: block.label,
               x: block.x, 
               blockType: block.blockType, 
               fieldMappings: block.fieldMappings,
               requestData: block.requestData,
               expressionItems: block.expressionItems,
               filterItems: block.filterItems,
               parentBlockId: block.parentBlockId
                 
               });   
  }
  return blocks; 
  
}

function getScribeMaps(org, solution){
    var url = APIURL + org.id + "/solutions/" + solution.id + "/maps/"; 
    var primaryContact; 
    var maps =[]; 
    var map; 
    var dataSet = []; 
    
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
       
  for (var i = 0; i < dataSet.length; i++) {
    map = dataSet[i];
    
    if (map.valid == true){
    maps.push({id: map.id, name: map.name, lastModificationDate: map.lastModificationDate}); 
    }
  }
  return maps; 
}


function getScribeOrg(orgId){
    var url = APIURL + orgId; 
    var primaryContact; 
    var org; 
    var data;
    var dataSet;
    var method; 
    
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
  
    primaryContact = dataSet.primaryContactFirstName + " " + dataSet.primaryContactLastName; 
    
    org = {id: dataSet.id, name: dataSet.name, primaryContact: primaryContact, parentId: dataSet.parentId};   
 
  return org; 
}

function getScribeOrgs(){
    var url = APIURL; 
    var primaryContact; 
    var orgs =[]; 
    var org; 
    var dataSet = []; 
    
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
    
    for (var i = 0; i < dataSet.length; i++) {
    org = dataSet[i];
    primaryContact = org.primaryContactFirstName + ' ' + org.primaryContactLastName
    //Push a simple ORG object
    orgs.push({id: org.id, name: org.name, primaryContact: primaryContact,parentId: org.parentId });   
    }
  
  return orgs; 
}
               
function getScribeSolution(org, solutionId){
    
    var solution; 
    var lastmodifiedUser; 
    var lastmodified;  
    var dataSet; 
    var data; 
    var method; 
    var mapLinks = []; 

    var url = APIURL + org.id + "/solutions/" + solutionId;
    method = "Get"; 
    dataSet = callScribeApi(url, method);   
  
    if (dataSet.solutionType != "Replication"){
      
      //Iternate the Maplinks and get the last mod rolling up at the solution from the Maps Collection 
      lastmodified = "01/01/2000"; 
      mapLinks = dataSet.mapLinks; 
      for (var ml = 0; ml < mapLinks.length; ml++) { 
        if (lastmodified < mapLinks[ml].lastModified){
            lastmodifiedUser = mapLinks[ml].modifiedBy;
            lastmodified = mapLinks[ml].lastModified;  
        }
      }
    }
    else {
      lastmodifiedUser = "RS - N/A"; 
      lastmodified = "RS - N/A"; 
    } 
    
  
  solution = {id: dataSet.id, name: dataSet.name,  status: dataSet.status, lastRunTime: dataSet.lastRunTime, nextRunTime: dataSet.nextRunTime, modifiedBy: lastmodifiedUser, lastModified: lastmodified};    
  return solution;                    
  }


function getScribeSolutionsListByOrgId(orgId){
    var url = APIURL + orgId + "/solutions" ;
    var solutions = []; 
    var solution; 
    var dataSet; 
    var method; 
    
    method = "Get"; 
    dataSet = callScribeApi(url, method); 
  
    for (var i = 0; i < dataSet.length; i++) {
    solution = dataSet[i];
    
    solutions.push({id: solution.id, name: solution.name}); 
  }
  
  return solutions;                    
  }

//=============================  OUTPUT STATEMENTS  =====================================


function outputBlocks(blocks){
  //TODO:  Add a function to order blocks by parentID (double linked list required)
  var offset; 
  var flowOffset; 
  var columnCount; 
  var rows= []; 
  var block; 
  
for (var z = 0; z < blocks.length; z++){
      block = blocks[z];
      flowOffset == 1; 
      ss = SpreadsheetApp.getActiveSpreadsheet();
      sheet = ss.getActiveSheet();
        
    switch (block.blockType) {
      case "Upsert":
      case "BuildReply":
      case "Update":
      case "Insert":
        offset = 3;
        columnCount = 3;
        sheet.appendRow([block.label, block.entity, block.blockType,block.name ]);
        rows = outputFieldMap(block); 
        break;
        
      
      case  "WaitForRequest":
        offset = 3;
        columnCount = 3;
        sheet.appendRow([block.label, block.entity, block.blockType,block.name]); 
        rows = outputRequestData(block);
        break;
        
      case  "ForEach":
      case  "ForEachChild": 
        offset = 3;
        columnCount = 1;
        sheet.appendRow([block.label, block.blockType,block.name ]); 
        rows = outputForEachData(block);
        break;
      
      case  "Group":   
        offset = 3;
        columnCount = 1;
        sheet.appendRow([block.label, block.blockType,block.name ]); 
        rows = outputGroupData(block);
        break;
        
        
      case  "IfElse":
        offset = 3;
        columnCount = 4;
        sheet.appendRow([block.label, block.blockType,block.name]);
        rows = outputIfElse(block);
        break;
         
      case  "Fetch":
      case  "Query":
      case  "Lookup":
        offset = 3;
        columnCount = 4;
        sheet.appendRow([block.label, block.blockType,block.name ]);
        rows = outputFilterItems(block);
        break;

      case  "SendReply":
        offset = 3;
        columnCount = 1;
        rows = outputSendReply(block);
        break;
    default: 
        //ToDO: add a default;
    }
        
        //output sheet details
        insertRows(offset, rows, columnCount) 
        
       }
 
}

function outputFieldMap(block){  
         var rows = []; 
         var i; 
         var fieldMap; 
         var fieldMappings =[];
  
         fieldMappings = block.fieldMappings; 
        
         for (i =0; i < fieldMappings.length; i ++){
                   fieldMap = fieldMappings[i]; 
                 
         rows.push([fieldMap.targetDataType, 
                    fieldMap.targetField, 
                    fieldMap.targetFormula]);  
 
                   }
         return rows; 
         }

function outputSendReply(block){  
         var rows = []; 
         rows.push(["Send Reply from Endpoint Service"]);         
         return rows; 
         }

function outputForEachData(block){  
         var rows = []; 
         rows.push([block.name]);         
         return rows; 
         }

function outputGroupData(block){  
         var rows = []; 
         rows.push(["'" + block.description + "'"]);         
         return rows; 
         }

function outputRequestData(block){  
         var rows = []; 
         var i; 
         var requestData =[]; 
         var request;
  
    
          requestData = block.requestData; 
          
          for (var i =0; i < requestData.length; i ++){
            request = requestData[i]; 
            
            rows.push([request.name, 
                       request.dataType, 
                       request.sampleValue]);  
 
             }          
         return rows; 
         }

function outputIfElse(block){  
         var rows = []; 
         var i;  
         var expressionItem;
         var expressionItems = [];
        
         expressionItems = block.expressionItems; 
         
     if (expressionItems.length == 0){
        rows.push(["no expression item paramaters", "", "", ""]); 
    
     }
    
     else {
  
          for (var i =0; i < expressionItems.length; i ++){
            expressionItem = expressionItems[i]; 
                   
            rows.push([expressionItem.conditionOperator, 
                       expressionItem.leftExpression, 
                       expressionItem.logicalOperator,
                       expressionItem.rightExpression]);  
 
             }     
         }
         return rows; 
    
    }

function outputFilterItems(block){  
         var rows = []; 
         var i;  
         var filterItem;
         var filterItems =[]; 
         var logicalOperator; 
         Logger.log("In Filter Items"); 
         filterItems = block.filterItems
       
   if (filterItems.length == 0){
     rows.push(["no filter paramaters", "", "", ""]); 
     }
  
  else {
          for (var i =0; i < filterItems.length; i ++){
            filterItem = filterItems[i];         
            rows.push([filterItem.fieldName, 
                       filterItem.comparisonOperator, 
                       filterItem.compareValue,
                       filterItem.logicalOperator]);  
 
             }   
    } 
    return rows; 
}

function outputOrg(org) { 
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheets = ss.getSheets();
  var sheet = ss.getActiveSheet();
  var primaryContact; 
  var rows = []; 
  var hdr = []; 
  var offset = 1; 
  var columnCount = 2;
  
  sheet.setName("Org Details"); 
  
  //Setup the ORG Sheet Headers
  hdr.push(['ID','NAME']); 
  insertHeader(offset, hdr, columnCount); 
  rows.push([org.id, org.name]); 
  
  if (rows.length > 0){
   Logger.log("Pushing Rows = " + rows.length); 
   insertRows(offset, rows, columnCount);   
  }
}

function outputMap(map){
  //Output Basic Data
    var hdr = []; 
    createSheet(map.name); 
    offset = 1;
    columnCount = 3; 
    hdr.push(['Map Name','Map Id','LAST MODIFIED']); 
    insertHeader(offset, hdr, columnCount); 
    var rows = []; 
    rows.push([map.name, map.id, map.lastModificationDate]); 
    insertRows(offset, rows, columnCount);  
}

function outputSolution(solution){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(solution.name);
  var hdr = []; 
  var rows = []; 
  var offset = 1; 
  var columnCount = 6; 
  
  //Setup the Header in the Sheet 
  hdr.push(['ID a','NAME','STATUS','LAST RUN','MODIFIED BY','LAST MODIFIED']); 
  insertHeader(offset, hdr, columnCount); 
  
  //Code to update the Spreadsheet
  
    rows.push([solution.id, 
               solution.name, 
               solution.status, 
               solution.lastRunTime, 
               solution.modifiedBy, 
               solution.lastModified]); 
   
  if (rows.length > 0){
   insertRows(offset, rows, columnCount);   
  }
}
  
function outputSolutions(org, solutions){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheets = ss.getSheets();
  var sheet = ss.getSheetByName(org.name);
  var rows = []; 
  var solution; 
  var hdr = []; 
  var offset = 1; 
  var columnCount = 8;
  
  //Setup the Header in the Sheet 
  hdr.push(['ID','NAME','STATUS','LAST RUN','MODIFIED BY','LAST MODIFIED']); 
 insertHeader(offset, hdr, columnCount); 
  
  //Add the Values 
  for (var i = 0; i < solutions.length; i++) {
    solution = solutions[i]; 
    
    rows.push([solution.id, 
               solution.name, 
               solution.status, 
               solution.lastRunTime, 
               solution.modifiedBy, 
               solution.lastModified]);  
  }
  
  if (rows.length > 0){
   insertRows(offset, rows, columnCount);   
  }  
}

function createSheet(sheetName){
    var sheet;
    var org; 
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    ss.insertSheet(sheetName);
    sheet = ss.getSheetByName(sheetName); 
    sheet.activate(); 
 }
  

//=================================  Utilities Section  =================================== 

//Build Headers for Spreadsheet
function insertHeader(offset, hdr, columnCount) {
  //TODO: Update with better nesting and offset capabilities 
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheets = ss.getSheets();
  var sheet = ss.getActiveSheet();
  var lastRow; 
  
  if (sheet.getLastRow == 0){
   lastRow = 1} 
  else {
   lastRow = sheet.getLastRow();
   lastRow += 1;
  } 
  dataRange = sheet.getRange(lastRow, offset, hdr.length, columnCount); 
  dataRange.setValues(hdr);
 }

function insertRows(offset, rows, columnCount) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheets = ss.getSheets();
  var sheet = ss.getActiveSheet();
  
  var lastRow; 
  
  if (sheet.getLastRow == 0){
   lastRow = 1} 
  else {
   lastRow = sheet.getLastRow();
   lastRow += 1;
  } 
  dataRange = sheet.getRange(lastRow, offset, rows.length, columnCount); 
  dataRange.setValues(rows);
 }

//Reset the Spreadsheet Between Runs for Ease of Use 
function doReset(){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var a = ss.getSheets();
  
  for (i = 0; i < a.length; ++i) {
    //Loop the sheets array and activate the current sheet
    a[i].activate(); 
    a[i].clear();
   
    //Do not delete the first sheet 
    if (i > 0){ 
       ss.deleteActiveSheet(); 
     }
    
  }
}

function callScribeApi(APIURL, method){ 
  
  Logger.log("Inside of Scribe Call API"); 
  var headers = { "Authorization" : "Basic " + Utilities.base64Encode(USER.username + ':' + USER.password)};
  var params = {"method":method,"headers":headers,"muteHttpExceptions": true,"contentType":"application/json", "followRedirects":true};  
  
  try
  {
    response = UrlFetchApp.fetch(APIURL, params);
    var resGetContentText = response.getContentText()
    Logger.log("Response = " + resGetContentText); 
    responseData = JSON.parse(resGetContentText);
    Logger.log("This is the Exception = " + responseData); 
    
  }
  catch(err)
  {
    Logger.log("This is the error trap response: " + err);
    Logger.log("Reponse Code = " + response.getResponseCode()); 
    Logger.log("response body = " + response.getContentText()); 
  }
  
   
  return responseData;  
} 
  